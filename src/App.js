import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import CreateArticle from './components/article/createArticle';
import UpdateArticle from './components/article/updateArticle';
import AllArticles from './components/article/allArticles';


class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <div className="pixelplex-app">
                    <Switch>
                        <Route exact path='/v1/articles/create' component={CreateArticle}/>
                        <Route exact path='/v1/articles/:id/edit' component={UpdateArticle}/>
                        <Route exact path='/v1/articles:page?:limit?' component={AllArticles}/>
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}


export default App;
