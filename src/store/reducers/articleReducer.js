import { element } from "prop-types";

const initState = {
    articles:[]
};

const articleReducer = (state = initState, action) => {

    switch (action.type) {
        case 'GET_ALL_ARTICLES':
            return {
                ...state,
                articles: action.response.data.articles
            };
        case 'CREATE_ARTICLE':
            return {
                ...state,
                articles: [...state.articles,action.response.data]
            };
        case 'UPDATE_ARTICLE':
            return {
                ...state,
                articles: state.articles.map((el)=>{
                    return el._id==action.response.data._id?el=action.response.data:el;
                })
            };
        case 'GET_ALL_ARTICLES_ERROR','CREATE_ARTICLE_ERROR':
            return state;
        default:
            return state;
    }
};

export default articleReducer;