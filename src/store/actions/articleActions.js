import axios from "axios";

export const createArticle = (article) => {
    return async (dispatch) => {
        try {
            console.log(article);
            const response = await axios.post(`http://localhost:8080/v1/articles`, article);
            console.log(response);
            dispatch({type: 'CREATE_ARTICLE', response});
        } catch (error) {
            dispatch({type: 'CREATE_ARTICLE_ERROR'});
        }
    }
};

export const updateArticle = (article) => {
    return async (dispatch) => {
        try {
            console.log(article)
            const response = await axios.put(`http://localhost:8080/v1/articles/${article.id}`, article);
            console.log(response);
            dispatch({type: 'UPDATE_ARTICLE', response});
        } catch (error) {
            dispatch({type: 'UPDATE_ARTICLE_ERROR'});
        }
    }
};

export const getAllArticles = (data) => {
    return async (dispatch) => {
        try {
            if(data.page&&data.limit){
                const response = await axios.get(`http://localhost:8080/v1/articles?page=${data.page}&limit=${data.limit}`);
                dispatch({type: 'GET_ALL_ARTICLES', response});    
            }else if(data.page&&!data.limit){
                const response = await axios.get(`http://localhost:8080/v1/articles?page=${data.page}`);
                dispatch({type: 'GET_ALL_ARTICLES', response});    
            }else if(!data.page&&data.limit){
                const response = await axios.get(`http://localhost:8080/v1/articles?limit=${data.limit}`);
                dispatch({type: 'GET_ALL_ARTICLES', response});    
            }else {
                const response = await axios.get(`http://localhost:8080/v1/articles`);
                dispatch({type: 'GET_ALL_ARTICLES', response});      
            }
        } catch (error) {
            dispatch({type: 'GET_ALL_ARTICLES_ERROR'});
        }
    }
};