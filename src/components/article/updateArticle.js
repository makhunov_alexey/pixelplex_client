import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateArticle } from '../../store/actions/articleActions';

class UpdateArticle extends Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.updateArticle({
            title:this.form.title.value,
            body:this.form.body.value,
            id:this.props.match.params.id,
        });
        this.props.history.push('/v1/articles');
    };

    render() {
        return (
            <div className="container">
            <h5>Edit</h5>
                <form onSubmit={this.handleSubmit} ref={el => (this.form = el)}>
                    <div className="form-group">
                        <label htmlFor="title">Title:</label>
                        <input type="text" className="form-control" id="title" aria-describedby="titlehelp" placeholder="Enter title" required></input>
                        <small id="emailHelp" className="form-text text-muted">Title is required!</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="body">Body:</label>
                        <textarea type="text" className="form-control" id="body" placeholder="Article body..." required></textarea>
                    </div>
                    <div className="input-field">
                        <button className="btn btn-outline-dark">Create</button>
                        <a href='/v1/articles' className="btn btn-outline-dark">Cancel</a>
                    </div>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateArticle: (article) => dispatch(updateArticle(article)),
    }
};

export default connect(null, mapDispatchToProps)(UpdateArticle);
