import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllArticles } from '../../store/actions/articleActions';
import {Button, Modal} from 'react-bootstrap'
import * as qs from 'query-string';

class AllArticles extends Component {
    constructor(props, context) {
        super(props, context);
    
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    
        this.state = {
          show: false,
        };
      }
    
    handleClose() {
        this.setState({ show: false });
    }
    
    handleShow() {
        this.setState({ show: true });
    }

    componentDidMount() {
        this.props.getAllArticles(qs.parse(this.props.location.search));
    }

    render() {
        const {articles} = this.props;
        return (
            <div className="container">
            <h1>Articles</h1>
            <a href='/v1/articles/create' className="createMainPage">Create</a>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>id</th>
                    <th>title</th>
                    <th>body</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    {articles && articles.map(article => {
                        return(
                        <tr>
                        <th>{article._id}</th>
                        <td>{article.title}</td>
                        <td>{article.body}</td>
                        <td>
                            <a href={'/v1/articles/'+article._id+'/edit'} className="btn btn-outline-dark">edit</a>
                            
                            <Button  onClick={this.handleShow} variant="secondary">View</Button>

                            <Modal {...this.props} size="lg" aria-labelledby="contained-modal-title-vcenter"
                            centered show={this.state.show} onHide={this.handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>{article.title}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>{article.body}</Modal.Body>
                            <Modal.Footer>
                                <div className="modalData">
                                    <div className="modalDataCreated">
                                        <span>Created: </span>{article.created_at}
                                    </div>
                                    <div className="modalDataUpdated">
                                        <span>Updated: </span>{article.updated_at}
                                    </div>
                                </div>
                            </Modal.Footer>
                            </Modal>
                        </td>
                        </tr>)
                    })}
                </tbody>
                </table>
                <div className="page">
                    <a href={'/v1/articles?page='+(+(qs.parse(this.props.location.search).page)-1)+(qs.parse(this.props.location.search).limit?'&limit='+qs.parse(this.props.location.search).limit:'')}>{qs.parse(this.props.location.search).page>1?'prev':''}</a>
                    <a href={'/v1/articles?page='+(qs.parse(this.props.location.search).page>=2?qs.parse(this.props.location.search).page-1:1)+(qs.parse(this.props.location.search).limit?'&limit='+qs.parse(this.props.location.search).limit:'')}>{qs.parse(this.props.location.search).page>=2?qs.parse(this.props.location.search).page-1:1}</a>
                    <a href={'/v1/articles?page='+(qs.parse(this.props.location.search).page>=2?qs.parse(this.props.location.search).page:2)+(qs.parse(this.props.location.search).limit?'&limit='+qs.parse(this.props.location.search).limit:'')}>{qs.parse(this.props.location.search).page>=2?qs.parse(this.props.location.search).page:2}</a>
                    <a href={'/v1/articles?page='+(qs.parse(this.props.location.search).page>=2?+qs.parse(this.props.location.search).page+1:3)+(qs.parse(this.props.location.search).limit?'&limit='+qs.parse(this.props.location.search).limit:'')}>{qs.parse(this.props.location.search).page>=2?+qs.parse(this.props.location.search).page+1:3}</a>
                    <a href={'/v1/articles?page='+(+(qs.parse(this.props.location.search).page)+1)+(qs.parse(this.props.location.search).limit?'&limit='+qs.parse(this.props.location.search).limit:'')}>next</a>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        articles: state.article.articles
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAllArticles: (params) => dispatch(getAllArticles(params)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AllArticles);
