import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createArticle } from '../../store/actions/articleActions';

class CreateArticle extends Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.createArticle({
            title:this.form.title.value,
            body:this.form.body.value
        });
        this.props.history.push('/v1/articles');
    };

    render() {
        return (
            <div className="container">
                <h5>Create:</h5>
                <form onSubmit={this.handleSubmit} ref={el => (this.form = el)}>
                    <div className="form-group">
                        <label htmlFor="title">Title:</label>
                        <input type="text" className="form-control" id="title" aria-describedby="titlehelp" placeholder="Enter title" ></input>
                        <small id="emailHelp" className="form-text text-muted">Title is required!</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="body">Body:</label>
                        <textarea type="text" className="form-control" id="body" placeholder="Article body..." required></textarea>
                    </div>
                    <div className="input-field">
                        <button className="btn btn-outline-dark">Create</button>
                        <a href='/v1/articles' className="btn btn-outline-dark">Cancel</a>
                    </div>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createArticle: (article) => dispatch(createArticle(article)),
    }
};

export default connect(null, mapDispatchToProps)(CreateArticle);
